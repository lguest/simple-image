<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token', function (Blueprint $table) {
            $table->bigIncrements('token_no');
            $table->integer('mall_no')->nullable(false);
            $table->string('mall_id')->nullable(false);
            $table->string('user_id');
            $table->string('access_token')->nullable(false);;
            $table->dateTimeTz('expires_at')->nullable(false);;
            $table->string('refresh_token')->nullable(false);;
            $table->dateTimeTz('refresh_token_expires_at')->nullable(false);;
            $table->dateTimeTz('issued_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token');
    }
}
