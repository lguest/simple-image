<?php

namespace App\Repositories;

interface TokenRepositoryInterface extends RepositoryInterface
{
    /**
     * 몰별 토큰정보 검색
     * @param $mall_id
     * @return mixed
     */
    public function getTokenInfo($mall_id);

    /**
     * 리프레시 토큰 검색
     * @param $mall_id
     * @return mixed
     */
    public function getRefreshToken($mall_id);
}
