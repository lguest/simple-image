<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    /**
     * 단건 검색
     * @param int $id
     * @return array
     */
    public function get($id)
    {
        return User::find($id);
    }

    /**
     * 유저 정보 검색
     * @param $user_id
     * @param array $userInfo
     * @return mixed
     */
    public function getUserInfo($user_id, $userInfo = array())
    {
        return User::firstOrCreate([
            'user_id' => $user_id
        ], $userInfo);
    }

    public function isExistsUser($mall_id, $user_id)
    {
        // TODO: Implement isExistsUser() method.
        $userInfo = User::where([
            'mall_id' => $mall_id,
            'user_id' => $user_id
        ])->first();

        return $userInfo !== null;
    }

    /**
     * 전체 검색
     *
     * @return mixed
     */
    public function all()
    {
        return User::all();
    }

    /**
     * 유저 정보 저장
     * @param $data
     * @return mixed|void
     */
    public function insert($data)
    {
        $result = User::create($data);

        return $result->id;
    }
    /**
     * 삭제
     *
     * @param int
     */
    public function delete($id)
    {

    }

    /**
     * 업데이트
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {

    }
}
