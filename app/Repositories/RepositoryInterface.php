<?php

namespace App\Repositories;

interface RepositoryInterface
{
     /**
     * Get's a post by it's ID
     *
     * @param mixed $id
     */
    public function get($id);

    /**
     * Get's all posts.
     *
     * @return mixed
     */
    public function all();

    /**
     * Insert
     * @param $data
     * @return mixed
     */
    public function insert($data);

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($id);

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_data);
}
