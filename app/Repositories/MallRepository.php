<?php

namespace App\Repositories;

use App\Models\Mall;

class MallRepository implements MallRepositoryInterface
{
    /**
     * 단건 검색
     * @param int $id
     * @return array
     */
    public function get($id)
    {
        return Mall::find($id);
    }

    /**
     * 가입된 몰인지 확인
     * @param string $mall_id
     * @return bool
     */
    public function isExistsMall(string $mall_id) {
        if( Mall::where('mall_id', $mall_id)->first() === null) {
            return false;
        }
        return true;
    }

    /**
     * 몰 정보 반환 없을 시 insert
     * @param string $mall_id
     * @return mixed
     */
    public function getMallInfo(string $mall_id)
    {
        return Mall::firstOrCreate([
            'mall_id' => $mall_id,
            'shop_name' => $mall_id
        ]);
    }

    /**
     * 전체 검색
     *
     * @return mixed
     */
    public function all()
    {
        return Mall::all();
    }

    /**
     * 몰 정보 입력
     * @param $data
     * @return mixed
     */
    public function insert($data)
    {
        $result = Mall::create($data);

        return $result->id;
    }
    /**
     * 삭제
     *
     * @param int
     */
    public function delete($id)
    {

    }

    /**
     * 업데이트
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {

    }
}
