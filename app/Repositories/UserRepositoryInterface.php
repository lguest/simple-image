<?php

namespace App\Repositories;

interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * 유저 정보 검색
     * @param $user_id
     * @param $userInfo
     * @return mixed
     */
    public function getUserInfo($user_id, $userInfo);

    /**
     * 가입된 유저인지 확인
     * @param $mall_id
     * @param $user_id
     * @return mixed
     */
    public function isExistsUser($mall_id, $user_id);
}
