<?php

namespace App\Repositories;

use App\Models\Token;

class TokenRepository implements TokenRepositoryInterface
{
    /**
     * 단건 검색
     * @param int $id
     * @return array
     */
    public function get($id)
    {
        return Token::find($id);
    }


    /**
     * 현재 활성화 되어있는 토큰정보 반환
     * @param $mall_id
     * @return mixed
     */
    public function getTokenInfo($mall_id)
    {
        return Token::where([
            ['mall_id', '=', $mall_id],
            ['expires_at', '>', now()]
        ])->first();
    }

    /**
     * 리프레시 토큰 정보 반환
     * @param $mall_id
     * @return mixed
     */
    public function getRefreshToken($mall_id)
    {
        // TODO: Implement getRefreshToken() method.
        $tokenInfo = Token::where([
                ['mall_id', '=', $mall_id],
                ['refresh_token_expires_at', '>', now()]
        ])->orderBy('token_no', 'DESC')->first();

        if ($tokenInfo !== null) {
            return $tokenInfo->refresh_token;
        }
        return null;
    }

    /**
     * 전체 검색
     *
     * @return mixed
     */
    public function all()
    {
        return Token::all();
    }
    /**
     * 인서트
     */
    public function insert($data)
    {
        $token = new Token;

        $token->mall_id = $data['mall_id'];
        $token->user_id = $data['user_id'];
        $token->access_token = $data['access_token'];
        $token->expires_at = $data['expires_at'];
        $token->refresh_token = $data['refresh_token'];
        $token->refresh_token_expires_at = $data['refresh_token_expires_at'];
        $token->issued_at = $data['issued_at'];

        $token->save();

    }
    /**
     * 삭제
     *
     * @param int
     */
    public function delete($id)
    {

    }

    /**
     * 업데이트
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {

    }
}
