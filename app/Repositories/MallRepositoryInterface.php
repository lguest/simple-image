<?php

namespace App\Repositories;

interface MallRepositoryInterface extends RepositoryInterface
{
    /**
     * 몰 아이디로 몰정보 반환
     * @param string $mall_id
     * @return mixed
     */
    public function getMallInfo(string $mall_id);

    /**
     * 가입된 몰인지 확인
     * @param string $mall_id
     * @return boolean
     */
    public function isExistsMall(string $mall_id);
}
