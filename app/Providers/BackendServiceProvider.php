<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $repositoryName = [
            'User', 'Token', 'Mall'
        ];

        foreach ($repositoryName as $name) {

            $this->app->bind(
                "App\\Repositories\\{$name}RepositoryInterface",
                "App\\Repositories\\{$name}Repository"
            );
        }
    }
}
