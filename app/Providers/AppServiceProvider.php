<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //카페24 프로바이더 등록
        $this->bootCafe24Socialite();
    }

    private function bootCafe24Socialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'cafe24',
            function ($app) use ($socialite) {
                $config = $app['config']['services.cafe24'];
                return $socialite->buildProvider(\App\Providers\Cafe24Provider::class, $config);
            }
        );
    }
}
