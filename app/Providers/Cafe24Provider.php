<?php

namespace App\Providers;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;
use Illuminate\Support\Str;

class Cafe24Provider extends AbstractProvider implements ProviderInterface {

    /**
     * Scopes
     *
     * @var string
     */

    const SCOPE_READ_PRODUCT = 'mall.read_product';
    const SCOPE_WRITE_PRODUCT = 'mall.write_product';
    const SCOPE_READ_STORE = 'mall.read_store';

    protected $mall_id;
    protected $user_id;

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase(
            sprintf('https://%s.cafe24api.com/api/v2/oauth/authorize', $this->parameters['mall_id']),
            $state
        );
    }
    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return sprintf('https://%s.cafe24api.com/api/v2/oauth/token', $this->mall_id);
    }
    /**
     * {@inheritdoc}
     */
    public function getAccessToken($code)
    {
        $state = $this->request->session()->pull('state');
        $stateInfo = json_decode(base64_decode($state), true);
        $this->mall_id = $stateInfo['mall_id'];

        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)],
            'form_params' => $this->getTokenFields($code),
        ]);

        return $this->parseAccessToken($response->getBody());
    }

    /**
     * Refresh Access Token
     * @param $refresh_token
     * @return Array
     */
    public function refreshAccessToken ($refresh_token)
    {
        $this->mall_id = $this->parameters['mall_id'];

        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)],
            'form_params' => $this->getRefreshTokenFields($refresh_token),
        ]);

        return $this->parseAccessToken($response->getBody());
    }

    /**
     * Parse Access Token
     * @param $tokenInfo
     * @return mixed
     */
    public function parseAccessToken($tokenInfo)
    {
        return json_decode($tokenInfo, true);
    }
    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_add(
            parent::getTokenFields($code), 'grant_type', 'authorization_code'
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getRefreshTokenFields($refresh_token)
    {
        return [
            'refresh_token' => $refresh_token,
            'grant_type' => 'refresh_token'
        ];
    }
    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get(
            sprintf('https://%s.cafe24api.com/api/v2/admin/users', $this->parameters['mall_id']), [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'mall_id'       => $user['mall_id'],
            'user_id' => $user['user_id'],
        ]);
    }

    protected function getState()
    {
        $state = array(
            'mall_id' => $this->parameters['mall_id'],         // 고정
            'state'  => Str::random(40)     // 코드발급 이후 처리에 필요한 값 필요시 추가
        );

        return base64_encode(json_encode($state));
    }

}
