<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mall extends Model
{
    protected $table = 'mall';

    protected $primaryKey = 'mall_no';

    public $timestamps = true;

    // 필수값
    protected $fillable = [
        'mall_no',      //몰번호
        'mall_id',      //몰아이디
        'shop_name',    //샵이름
    ];

    // 수정보호
    protected $guarded = ['mall_no', 'mall_id', 'shop_name'];
}
