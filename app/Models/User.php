<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'user';

    protected $primaryKey = 'user_no';

    public $timestamps = true;

    // 필수값
    protected $fillable = [
        'user_no',                  //토큰번호
        'user_id',                  //몰번호
        'mall_no',                  //몰아이디
        'mall_id'
    ];

    // 수정보호
    protected $guarded = ['user_no', 'user_id', 'mall_no'];

    public function getAuthPassword() {
        return $this->mall_id;
    }
}
