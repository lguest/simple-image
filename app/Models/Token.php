<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'token';

    protected $primaryKey = 'token_no';

    public $timestamps = true;

    // 필수값
    protected $fillable = [
        'token_no',                 //토큰번호
        'mall_no',                  //몰번호
        'mall_id',                  //몰아이디
        'access_token',             //엑세스토큰
        'expires_at',               //엑세스토큰 만료일
        'refresh_token',            //리프레시 토큰
        'refresh_token_expires_at', //리프레시 토큰 만료일
        'issued_at'                 //토큰발급일
    ];

    // 수정보호
    protected $guarded = ['mall_no', 'mall_id', 'access_token', 'refresh_token'];

    // Date Column
    //protected $dateFormat = 'Y-m-dTH:i:s.u';

    //protected $dates = ['expires_at', 'refresh_token_expires_at'];
}
