<?php

namespace App\Http\Services;

use App\Repositories\MallRepositoryInterface as Mall;
use App\Repositories\TokenRepositoryInterface as Token;
use App\Repositories\UserRepositoryInterface as User;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class AuthService
{

    private $mall;
    private $token;
    private $user;

    public $mallInfo;
    private $tokenInfo = null;

    public function __construct(Mall $mall, Token $token, User $user)
    {
        $this->mall = $mall;
        $this->token = $token;
        $this->user = $user;
    }

    public function getMallInfo()
    {

    }

    public function insertMallInfo()
    {

    }

    public function getUserInfo()
    {

    }

    public function insertUserInfo()
    {

    }

    /**
     * Token 정보 검색
     * @param $mall_id
     * @return mixed
     */
    public function getTokenInfo($mall_id)
    {
        $this->tokenInfo = $this->token->getTokenInfo($mall_id);
        return $this->tokenInfo;
    }

    /**
     * 엑세스 토큰 정보 획득
     * @param $mall_id
     * @return mixed
     */
    public function getAccessToken ($mall_id)
    {
        if ($this->tokenInfo === null) {
            $this->getTokenInfo($mall_id);
        }

        return $this->tokenInfo->access_token;
    }

    /**
     * Access Token 갱신 처리
     * @param $mall_id
     * @return bool
     */
    public function getRefreshToken($mall_id)
    {
        return $this->token->getRefreshToken($mall_id);
    }

    public function insertTokenInfo($tokenInfo)
    {
        $this->token->insert($tokenInfo);
    }

    public function isExistsUser($mall_id, $user_id)
    {
        return $this->user->isExistsUser($mall_id, $user_id);
    }

    /**
     * 가입된 몰인지 체크
     * @param $mall_id
     * @return bool
     */
    public function isExistsMall($mall_id)
    {
        return $this->mall->isExistsMall($mall_id);
    }

    public function doLogin($tokenInfo)
    {
        $mall_id = $tokenInfo['mall_id'];
        $user_id = $tokenInfo['user_id'];

        // 몰 정보 검색
        $mallInfo = $this->mall->getMallInfo($mall_id);

        Log::debug('MALL INFO');
        Log::debug($mallInfo);

        // 유저 정보 검색
        $userInfo = $this->user->getUserInfo($user_id, [
            'mall_no' => $mallInfo['mall_no'],
            'mall_id' => $mallInfo['mall_id']
        ]);

        Log::debug('USER INFO');
        Log::debug($userInfo);

        // 로그인 처리 (세션저장)
        Auth::loginUsingId($userInfo->user_no);

        session()->put('mall_id', $mall_id);
        session()->put('user_id', $user_id);

        Log::debug('SESSION');
        Log::debug(session()->all());

        return redirect('/dashboard');
    }

}

