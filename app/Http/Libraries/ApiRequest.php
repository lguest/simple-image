<?php


namespace App\Http\Libraries;


use \GuzzleHttp\Client;


use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\GuzzleException;

use Illuminate\Support\Facades\Auth;
use Log;
/**
* Class ApiRequest
* 외부 api 호출용
* API Request Library
* @package App\Http\Libraries
*/
class ApiRequest
{
    private $client;
    private $headers = [];
    private $options = [];
    private $mall_id;

    public function __construct()
    {
        $this->mall_id = Auth::user()->mall_id;

        $this->client = new Client(
            ['base_uri' => sprintf(config('api.api_url'), $this->mall_id), 'connect_timeout' => 3.0]
        );

        $this->options['connect_timeout'] = 3.0;
        $this->options['debug'] = false;

        //기본 헤더
        $this->headers['Accept'] = 'application/json';
        $this->headers['Content-Type'] = 'application/json';
    }


    /**
     * GET 방식 호출
     * Basic Auth
     * @param string $uri
     * @param array $params
     * @param string $access_token
     * @return array
     */
    public function get(string $uri, string $access_token, $params = [])
    {
        try{

            $this->options['headers']['Authorization'] = 'Bearer '  . $access_token;
            $this->options['query'] = $params;

            $result = $this->client->request('GET', $uri, $this->options);


            return ['result' => true, 'data' => $result->getBody()->getContents(), 'message' => '정상적으로 처리되었습니다. '];


        }catch (ClientException $e){


            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }catch (GuzzleException $e) {


            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }


    }




    /**
     * POST 방식 호출
     * @param string $uri
     * @param array $params
     * @param string $auth
     * @param null $token
     * @return array
     */
    public function post(string $uri, $params = [], $token = null, $auth = 'bearer')
    {


        try{
            if($auth == 'bearer'){ //Bearer Auth
                $this->headers['Authorization'] = 'Bearer '  . $token;
            }else{ //Basic Auth
                $this->options['auth'] = [config('recipe.CLIENT_ID'), config('recipe.CLIENT_SECRET')];
            }


            $this->options['headers'] = $this->headers;


            $this->options['json'] = $params;


            $result = $this->client->request('POST', $uri, $this->options);


            $statusCode = $result->getStatusCode();
            if($statusCode != 200){
                throw new \Exception('전송 중 오류가 발생하였습니다. ');
            }


            return ['result' => true, 'data' => $result->getBody()->getContents(), 'message' => '정상적으로 처리되었습니다. '];


        }
        catch (\Exception $e){
            return ['result' => false, 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }catch (GuzzleException $e) {

            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }


    }


    /**
     * PUT 방식 호출
     * @param string $uri
     * @param array $params
     * @param string $auth
     * @param null $token
     * @return array
     */
    public function put(string $uri, $params = [],$token = null,  $auth = 'bearer')
    {


        try{
            if($auth == 'bearer'){ //Bearer Auth
                $this->headers['Authorization'] = 'Bearer '  . $token;
            }else{ //Basic Auth
                $this->options['auth'] = [config('recipe.CLIENT_ID'), config('recipe.CLIENT_SECRET')];
            }


            $this->options['headers'] = $this->headers;


            $this->options['json'] = $params;


            $result = $this->client->request('PUT', $uri, $this->options);


            $statusCode = $result->getStatusCode();
            if($statusCode != 200){
                throw new \Exception('전송 중 오류가 발생하였습니다. ');
            }


            return ['result' => true, 'data' => $result->getBody()->getContents(), 'message' => '정상적으로 처리되었습니다. '];


        }
        catch (\Exception $e){
            return ['result' => false, 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }catch (GuzzleException $e) {


            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }


    }


    /**
     * DELETE 방식 호출
     * Basic Auth
     * @param string $uri
     * @param array $params
     * @return array
     */
    public function delete(string $uri, $params = [], $access_token)
    {
        try{
            $this->headers['Authorization'] = 'Bearer '  . $access_token;
            $this->options['query'] = $params;

            $result = $this->client->request('DELETE', $uri, $this->options);


            return ['result' => true, 'data' => $result->getBody()->getContents(), 'message' => '정상적으로 처리되었습니다. '];


        }catch (ClientException $e){


            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }catch (GuzzleException $e) {


            return ['result' => false , 'message' => '전송중 오류가 발생하였습니다. ('.$e->getMessage().')'];
        }


    }


    /**
     * 최신 활성 트리거 리스트 가져와서 갱신
     * @param $channel_no
     * @param $user_id
     * @return array
     */
    public function getActiveTriggerList($channel_no, $user_id){


        $uri = 'channels/'.$channel_no.'/channel-users/'.$user_id.'/triggers';
        $result = $this->get($uri);


        return $result;
    }


    /**
     * 액션 결과 생성
     * @param $response
     * @return array
     */
    public function makeActionResult($response){


        return ['data' => [ 'id' => $response['id'] ] ];
    }


}
