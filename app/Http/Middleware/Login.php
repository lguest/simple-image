<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Repositories\UserRepositoryInterface as User;

use Log;

class Login
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param \Illuminate\Http\Request  $request
     * @param Closure $next
     * @param User $user
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 로그인 체크
        if (Auth::guard('web')->check()) {
            return $next($request);
        }

        return redirect()->route('login', $request);

    }
}
