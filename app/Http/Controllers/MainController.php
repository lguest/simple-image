<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\AuthService;
use App\Http\Libraries\ApiRequest;

class MainController extends Controller
{

    private $authService;
    private $user;
    private $access_token;

    public function __construct( AuthService $authService)
    {
        $this->middleware('login');

        $this->authService = $authService;
    }

    public function dashboard(Request $request, ApiRequest $api)
    {

        $mall_id = Auth::user()->mall_id;

        $access_token = $this->authService->getAccessToken($mall_id);

        $result = $api->get('products', $access_token);

        return View('dashboard', json_decode($result['data'], true));
    }
}
