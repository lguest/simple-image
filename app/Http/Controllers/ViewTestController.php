<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ViewTestController extends Controller
{

    // 의존성주입
    public function __construct()
    {

    }
    public function index()
    {
        return View('test');
    }

}
