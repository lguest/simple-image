<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\AuthService;

class OauthController extends Controller
{
    private $client_id;
    private $client_secret;
    private $redirect_url;
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->client_id = config('app.client_id');
        $this->client_secret = config('app.client_secret');
        $this->redirect_url = config('app.redirect_url');
        $this->authService = $authService;
    }
    /**
     * 인증 파라미터 검증
     */
    public function validateCertifyParams($request)
    {
        return true;
    }
    /**
     * hmac 검증
     */
    public function checkHmac($request)
    {
        return true;
    }

    /**
     * state 상태값 검증
     */
    public function checkState(Request $request)
    {
        if ($request->session()->get('state') !== $request->input('state')) {
            return false;
        }
        return true;
    }
    /**
     * 인증 검증 페이지
     * https://{{AppUrl}}/?is_multi_shop={{멀티쇼핑몰여부}}&lang={{쇼핑몰언어}}&
     * mall_id={{mall_id}} &
     * shop_no={{shop_no}}&
     * timestamp= {{timestamp}}&
     * user_id={{로그인아이디}}&
     * user_name={{로그인사용자이름}}
     * &user_type={{사용자유형}}
     * &hmac={{검증용 key}}
     *
     */
    public function certify(Request $request)
    {

        // 파라미터 검증
        if (!$this->validateCertifyParams($request->input())){
            die('잘못된 접속 입니다. ');
        }

        // hmac 검증 필요
        if (!checkHmac()){
            die('잘못된 접속 입니다. ');
        }

        $mall_id = $request->input('mall_id');
        $client_id = $this->client_id;

        $redirect_url = config('app.redirect_url');
        $scope = config('app.scope');

        $random_string = generateRandomString();
        //세션에 저장
        $request->session()->put('state', $random_string);

        $state = array(
            'mall_id' => $mall_id,         // 고정
            'state'  => $random_string      // 코드발급 이후 처리에 필요한 값 필요시 추가
        );
        // 이하 공통
        $authorize_url = sprintf(config('app.authorize_url') . '?', $mall_id);
        $request_data = array(
            'response_type' => 'code',
            'client_id'     => $client_id,
            'state'         => base64_encode(json_encode($state)),
            'redirect_uri'  => $redirect_url,
            'scope'         => $scope,
        );
        $url = $authorize_url . http_build_query($request_data);

        return redirect($url);
    }

    /**
     * $params code
     * $params state
     */
    public function callback(Request $request)
    {
        if (!$request->input('code') || $request->input(state)) {
            die('잘못된 접속 입니다. ');
        }

        $sClientId = $this->client_id;
        $sClientSecret = $this->client_secret;
        $sThisUrl = $this->redirect_url;   // https:// 사용
        $sCode = $request->code;//2번 응답의 'code'
        $state = $request->state;//2번 응답의 'state'
        // 이하 공통
        $state_info = json_decode(base64_decode($state), true);

        // state 검증처리
        if (!$this->checkState($request)) {
            die('잘못된 접속 입니다. ');
        }
        $post_fields = array(
            'grant_type'   => 'authorization_code',
            'code'         => $sCode,
            'redirect_uri' => $sThisUrl
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => sprintf(config('app.token_url') , $state_info['mall_id']),
            CURLOPT_POSTFIELDS     => http_build_query($post_fields),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER     => array(
                'Authorization: Basic ' . base64_encode($this->client_id . ':' . $this->client_secret)
            )
        ));

        $response = curl_exec($curl);
        // 정상 토큰발급 응답인지 확인용 출력해보기

        print_r($response);

        //토큰정보 저장
        $this->authService->insertTokenInfo($response);

        //메인페이지로 이동
        return redirect('/dashboard');
    }
}
