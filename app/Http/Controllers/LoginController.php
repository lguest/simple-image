<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Http\Services\AuthService;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

use App\Http\Requests\Login;
use App\Http\Requests\OauthCallback;

class LoginController extends Controller
{

    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function authenticate(Login $request)
    {
        if (!$request->validated()) {
            abort(422,'필수 파라미터 정보를 확인할 수 없습니다. ');
        }

        $mall_id = $request->input('mall_id');
        $user_id = $request->input('user_id');

        // 가입된 유저인지 확인
        if (!$this->authService->isExistsUser($mall_id, $user_id)) {
            // 인증 페이지로 리다이렉트
            return $this->requestAuthorizationCode($request);
        }

        //토크 정보 확인
        $tokenInfo = $this->authService->getTokenInfo($mall_id);
        if ($tokenInfo === null) {
            $refreshToken = $this->authService->getRefreshToken($mall_id);

            // 리프레시 토큰 검색
            if ($refreshToken == null) {
                return $this->requestAuthorizationCode($request);
            }

            // 재발급
            $tokenInfo = Socialite::driver('cafe24')
                ->with('mall_id', $mall_id)
                ->refreshAccessToken($tokenInfo['refresh_token']);
        }

        //로그인 처리
        return $this->authService->doLogin($tokenInfo);
    }

    /**
     * 인증 요청
     * @param Request $request
     * @return mixed
     */
    protected function requestAuthorizationCode(Request $request)
    {
        // Hmac 검증
        if (!checkHmac()) {
            abort(422,'Hmac 검증에 실패하였습니다. ');
        }

        $mall_id = $request->input('mall_id');

        return Socialite::driver('cafe24')
            ->with(['mall_id' => $mall_id])
            ->setScopes(['mall.read_product', 'mall.write_product', 'mall.read_product'])
            ->redirect();

    }

    /**
     * Oauth Callback
     * @param OauthCallback $request
     * @return RedirectResponse
     */
    public function callback(OauthCallback $request)
    {
        if (!$request->validated()) {
            abort('403', '잘못된 접속 입니다. ');
        }

        $code = $request->input('code');
        $tokenInfo = Socialite::driver('cafe24')->getAccessToken($code);

        Log::debug($tokenInfo);

        $this->authService->insertTokenInfo($tokenInfo);

        return $this->authService->doLogin($tokenInfo);
    }

    public function username()
    {
        return 'user_id';
    }

    public function logout()
    {
        echo '로그아웃 되었습니다. ';
        Auth::logout();
    }
}
