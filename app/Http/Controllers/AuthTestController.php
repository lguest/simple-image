<?php

namespace App\Http\Controllers;

use App\Http\Services\AuthService;
use Illuminate\Http\Request;


class AuthTestController
{
    private $authService;

    public function __construct(AuthService $authService)
    {

        $this->authService = $authService;
    }

    public function insertToken()
    {
        $token_data = [

            "access_token" => "{access_token}",
            "expires_at" => "{expiration_date_of_access_token}",
            "refresh_token" => "{refresh_token}",
            "refresh_token_expires_at" => "{expiration_date_of_refresh_token}",
            "client_id" => "{client_id}",
            "mall_id" => "{mall_id}",
            "user_id" => "{user_id}",
            "scopes" => [
                "mall.read_application",
                "mall.write_application",
                "mall.read_product",
                "mall.write_product",
                "mall.read_store"
            ],
            "issued_at" => "{issued_date}"

        ];

        $this->authService->insertTokenInfo($token_data);


    }

    public function saveToken(Request $request)
    {
        echo $request->input('user_id');

    }
}
