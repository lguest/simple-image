<?php
/**
 * API 관련 설정
 */

return [
    'client_id' => env('API_CLIENT_ID'),
    'client_secret' => env('API_CLIENT_SECRET'),
    'redirect_url' => 'https://simple-image-local.makemusic.kr/oauth/callback',
    'authorize_url' => 'https://%s.cafe24api.com/api/v2/oauth/authorize',
    'token_url' => 'https://%s.cafe24api.com/api/v2/oauth/token',
    'scope' => 'mall.read_product,mall.write_product,mall.read_store',
    'api_url' => 'https://%s.cafe24api.com/api/v2/admin/',
];
