대시보드
<style>
    table {border-left:1px solid #000000;border-top:1px solid #000000; }
    table th {border-right:1px solid #000000;border-bottom: 1px solid #000000}
    table td {border-right:1px solid #000000; border-bottom: 1px solid #000000}
</style>
<table>
    <thead>
    <th>상품번호</th>
    <th>상품이름</th>
    <th>상품이미지</th>
    </thead>
    <tbody>
@foreach ($products as $product)
    <tr>
        <td>{{$product['product_no']}}</td>
        <td>{{$product['product_name']}}</td>
        <td>
            @if ($product['small_image'] !== null)
                <img src="{{$product['small_image']}}" style="with:200px;height:200px"/>
            @endif
        </td>
    </tr>
@endforeach
    </tbody>
</table>
