<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// 메인 화면 > 대시보드
Route::get('/', 'MainController@dashboard');
Route::get('/dashboard', 'MainController@dashboard');

// 로그인 인증
Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@authenticate']);

// 로그아웃
Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

Route::group(['prefix' => 'oauth'], function () {
    //인증페이지
    Route::get('certify', 'OauthController@certify');
    //리다이렉트 페이지
    Route::get('callback', 'LoginController@callback');
});


//테스트 코드
Route::get('/test', 'ViewTestController@index');

Route::get('dbTest', 'TestController@dbTest');

Route::group(['prefix'=>'authTest'],function () {
    Route::get('insertToken','AuthTestController@insertToken');
    Route::post('insertToken', 'AuthTestController@saveToken');
});
